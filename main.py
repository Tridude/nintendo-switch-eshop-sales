#

import argparse
import os
import sqlite3
import json

import database

parser = argparse.ArgumentParser()
parser.add_argument("--json", nargs="?", type=str, default=None, help="path to json output")
args = parser.parse_args()

db = database.SaleDatabase("nsw_sale.db", 0)

db.cleanup_database()

if args.json is not None:
    f1 = open(os.path.join(args.json, "sales.json"), "w")
    f1.write(json.dumps(db.dump()))
    f1.close()
