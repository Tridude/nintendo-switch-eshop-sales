#

import requests
import json
import datetime
import sqlite3

import database

db = database.SaleDatabase("nsw_sale.db", database.id_eshopjp)

titles = []
sales_url = "https://ec.nintendo.com/api/JP/ja/search/sales?count={}&offset={}"
count = 30
prices_url = "https://ec.nintendo.com/api/JP/ja/mns_prices"
total = 0
offset = 0

# grab titles on sale
# print("get titles")
while offset <= total:
    # print(offset, total)
    try:
        r = requests.get(sales_url.format(count, offset))
        r.raise_for_status()
        j = json.loads(r.content.decode("UTF-8"))
        for t in j["contents"]:
            db.insert_title(t["id"], t["formal_name"], t["hero_banner_url"], "https://ec.nintendo.com/JP/ja/titles/{}".format(t["id"]))
            titles.append(t["id"])
        total = int(j["total"])
        offset += count
    except requests.HTTPError as e:
        print("HTTP error: {}".format(e))
        break
    except requests.exceptions.RequestException as e:
        print("Error in requests: {}".format(e))
        break
    except KeyError as e:
        print("Response JSON not in expected format. Can't find key: {}".format(e))
        break
    except sqlite3.Error as e:
        print("sqlite error: {}".format(e))
        break
    except Exception as e:
        print("Unknown error: {}".format(e))
        break

# grab sale prices
offset = 0
# print("get prices")
while offset < len(titles):
    # print(offset, len(titles))
    params = {"ns_uids": [t for t in titles[offset:offset+count]]}
    try:
        r = requests.get(prices_url, params=params)
        r.raise_for_status()
        j = json.loads(r.content.decode("UTF-8"))
        for t in j:
            db.insert_price(t["id"], t["price"]["regular_price"]["raw_value"], t["price"]["regular_price"]["currency"])
            db.insert_sale(t["id"], t["price"]["discount_price"]["raw_value"], t["price"]["discount_price"]["currency"], datetime.datetime.strptime(t["price"]["discount_price"]["start_datetime"], "%Y-%m-%dT%H:%M:%S%z").astimezone(), datetime.datetime.strptime(t["price"]["discount_price"]["end_datetime"], "%Y-%m-%dT%H:%M:%S%z").astimezone())
        offset += count
    except requests.HTTPError as e:
        print("HTTP error: {}".format(e))
        break
    except requests.exceptions.RequestException as e:
        print("Error in requests: {}".format(e))
        break
    except KeyError as e:
        print("Response JSON not in expected format. Can't find key: {}".format(e))
        break
    except sqlite3.Error as e:
        print("sqlite error: {}".format(e))
        break
    except Exception as e:
        print("Unknown error: {}".format(e))
        break

db.commit_database()
