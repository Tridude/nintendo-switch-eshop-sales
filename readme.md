
# Nintendo Switch Digital Sales Database
Scripts that parse various stores for Nintendo Switch digital game sales and dump them into a sqlite database.

Currently working parsers:

* eShop US
* eShop Japan

Parsers planned:

* Humble Store
* GameStop
* and more

Database usage example:

* Simple website - https://tridude.bitbucket.io/nsw_sales/
