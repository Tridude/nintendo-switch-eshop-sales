#

import os
import sqlite3
import datetime

id_eshopjp = 1
name_eshopjp = "eShop Japan"
id_eshopus = 2
name_eshopus = "eShop US"
id_humble = 3
name_humble = "Humble Store"

id_map = {
    id_eshopjp: name_eshopjp,
    id_eshopus: name_eshopus,
    id_humble: name_humble
}

class SaleDatabase(object):

    def __init__(self, db_path, store_id):
        self.store_id = store_id
        self.conn = self.initialize_db(db_path)
        self.conn.row_factory = sqlite3.Row
        self.cursor = self.conn.cursor()

    # todo - add another column in title table for link
    def initialize_db(self, db_path):
        if os.path.exists(db_path):
            conn = sqlite3.connect(db_path)
            conn.execute("PRAGMA foreign_keys = ON")
            return conn
        conn = sqlite3.connect(db_path)
        conn.execute("PRAGMA foreign_keys = ON")
        c = conn.cursor()
        # change titles to two primary keys?
        # name/id of title + store (jp/us/humble)
        c.execute("""
            CREATE TABLE stores (
                store_id INTEGER PRIMARY KEY,
                store_name TEXT
            )
        """)
        c.execute("""
            INSERT INTO stores (store_id, store_name) VALUES
            (?, ?),
            (?, ?),
            (?, ?)
        """, (id_eshopjp, name_eshopjp, id_eshopus, name_eshopus, id_humble, name_humble))
        c.execute("""
            CREATE TABLE titles (
                id TEXT,
                store_id INTEGER REFERENCES stores(store_id),
                name TEXT,
                banner_url TEXT,
                link TEXT,
                PRIMARY KEY (id, store_id)
            )
        """)
        c.execute("""
            CREATE TABLE prices (
                id TEXT,
                store_id INTEGER,
                price INTEGER,
                currency TEXT,
                FOREIGN KEY (id, store_id) REFERENCES titles(id, store_id) ON DELETE CASCADE,
                PRIMARY KEY (id, store_id)
            )
        """)
        c.execute("""
            CREATE TABLE sales (
                id TEXT,
                store_id INTEGER,
                price INTEGER,
                currency TEXT,
                start TIMESTAMP,
                end TIMESTAMP,
                FOREIGN KEY (id, store_id) REFERENCES titles(id, store_id) ON DELETE CASCADE,
                PRIMARY KEY (id, store_id)
            )
        """)
        conn.commit()
        return conn

    def insert_title(self, id, name, banner_url, link):
        self.cursor.execute("""
            INSERT OR REPLACE INTO titles
            (id, store_id, name, banner_url, link) values
            (?,  ?,        ?,    ?         , ?)
        """, (id, self.store_id, name, banner_url, link))

    def insert_price(self, id, price, currency):
        self.cursor.execute("""
            INSERT OR REPLACE INTO prices
            (id, store_id, price, currency) values
            (?,  ?,        ?,     ?       )
        """, (id, self.store_id, price, currency))

    def insert_sale(self, id, price, currency, start, end):
        self.cursor.execute("""
            INSERT OR REPLACE INTO sales
            (id, store_id, price, currency, start, end) values
            (?,  ?,        ?,     ?,        ?,     ?  )
        """, (id, self.store_id, price, currency, start, end))
    
    def commit_database(self):
        self.conn.commit()
    
    # return list of sales ending within the given datetime
    def get_sales_ending(self, end):
        self.cursor.execute("""
            SELECT t.name, t.store_id, t.id, s.price, s.currency, s.end
            FROM titles t JOIN sales s ON t.id = s.id
            WHERE s.end <= ?
        """, (end,))
        titles = []
        for row in self.cursor:
            titles.append((row[0], row[3], id_map[row[1]], row[5]))
        return titles

    def cleanup_database(self):
        now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S%z")
        c = self.conn.cursor()
        c2 = self.conn.cursor()
        # remove sales that have ended
        c.execute("""
            SELECT id, store_id FROM sales where end <= ? 
        """, (now,))
        for row in c:
            c2.execute("""
                DELETE FROM titles WHERE id = ? AND store_id = ?
            """, (row[0], row[1]))
        # remove orphaned titles that weren't properly added
        c.execute("""
            SELECT t.id, t.store_id FROM titles as t
            LEFT JOIN sales as s ON t.id = s.id AND t.store_id = s.store_id
            WHERE s.id is NULL
        """)
        for row in c:
            c2.execute("""
            DELETE FROM titles WHERE id = ? AND store_id = ?
            """, (row[0], row[1]))
        self.conn.commit()

    # returns the URL for the title
    # not used anymore
    def get_title_link(self, id, store_name):
        return "http://lmgtfy.com/?q={}+{}".format(store_name, id).replace(" ", "+")

    # returns a list of all sales for dumping as json
    def dump(self):
        self.cursor.execute("""
            SELECT
            t.id, st.store_name, t.name, t.banner_url, t.link, sa.price as sale_price, sa.currency, sa.start, sa.end, p.price as original_price
            FROM
            titles t
            JOIN sales sa ON sa.id = t.id AND sa.store_id = t.store_id
            JOIN prices p ON p.id = t.id AND p.store_id = t.store_id
            LEFT JOIN stores st ON st.store_id = t.store_id
        """)
        dump = []
        for row in self.cursor:
            row = dict(row)
            row["percent"] = int((1.0 - row["sale_price"] / row["original_price"]) * 100)
            dump.append(row)
        return dump
