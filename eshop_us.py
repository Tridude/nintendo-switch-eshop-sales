#

import requests
import json
import datetime
import sqlite3

import database

db = database.SaleDatabase("nsw_sale.db", database.id_eshopus)

titles = []
sales_url = "https://www.nintendo.com/json/content/get/filter/game?limit={}&offset={}&system=switch&sale=true&sort=release&direction=des"
count = 40
prices_url = "https://api.ec.nintendo.com/v1/price"
total = 0
offset = 0

# grab titles on sale
# print("get titles")
while offset <= total:
    # print(offset, total)
    try:
        r = requests.get(sales_url.format(count, offset))
        r.raise_for_status()
        j = json.loads(r.content.decode("UTF-8"))
        for t in j["games"]["game"]:
            db.insert_title(t["nsuid"], t["title"], t["front_box_art"], "https://www.nintendo.com/games/detail/{}".format(t["slug"]))
            titles.append(t["nsuid"])
        total = int(j["filter"]["total"])
        offset += count
    except requests.HTTPError as e:
        print("HTTP error: {}".format(e))
        break
    except requests.exceptions.RequestException as e:
        print("Error in requests: {}".format(e))
        break
    except KeyError as e:
        print("Response JSON not in expected format. Can't find key: {}".format(e))
        break
    except sqlite3.Error as e:
        print("sqlite error: {}".format(e))
        break
    except Exception as e:
        print("Exception type:{}\n{}".format(type(e).__name__, e))
        break

# grab sale prices
offset = 0
# print("get prices")
while offset < len(titles):
    # print(offset, len(titles))
    params = {"country": "US", "lang": "en", "ids": ",".join(titles[offset:offset+count])}
    try:
        r = requests.get(prices_url, params=params)
        r.raise_for_status()
        j = json.loads(r.content.decode("UTF-8"))
        for t in j["prices"]:
            try:
                db.insert_price(t["title_id"], t["regular_price"]["raw_value"], t["regular_price"]["currency"])
                db.insert_sale(t["title_id"], t["discount_price"]["raw_value"], t["discount_price"]["currency"], datetime.datetime.strptime(t["discount_price"]["start_datetime"], "%Y-%m-%dT%H:%M:%S%z").astimezone(), datetime.datetime.strptime(t["discount_price"]["end_datetime"], "%Y-%m-%dT%H:%M:%S%z").astimezone())
            except KeyError as e:
                # creates orphaned titles, but we'll delete them in db.cleanup_database()
                pass
        offset += count
    except requests.HTTPError as e:
        print("HTTP error: {}".format(e))
        break
    except requests.exceptions.RequestException as e:
        print("Error in requests: {}".format(e))
        break
    except KeyError as e:
        print("Response JSON not in expected format. Can't find key: {}".format(e))
        break
    except sqlite3.Error as e:
        print("sqlite error: {}".format(e))
        break
    except Exception as e:
        print("Unknown error: {}".format(e))
        break

db.commit_database()
